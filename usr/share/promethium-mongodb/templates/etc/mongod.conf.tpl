#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

net:
    bindIp: localhost,%%HOSTNAME%%
    port: 27017
    #tls:
    #    mode: requireTLS
    #    certificateKeyFile: /etc/ssl/mongodb.pem
    #    CAFile: /etc/ssl/caToValidateClientCertificates.pem
#DISTRIBUTED
replication: #DISTRIBUTED
    replSetName: promethiumMongo0 #DISTRIBUTED
    #enableMajorityReadConcern: true #DISTRIBUTED

storage:
    dbPath: /var/lib/mongodb
    journal:
        enabled: true
      
systemLog:
    destination: file
    path: "/var/log/mongodb/mongod.log"
    logAppend: true
    logRotate: reopen
   
#security: #SECURITY
#    keyFile: /etc/mongo/mongo-keyfile #SECURITY
#    clusterAuthMode: keyFile #SECURITY
#    authorization: enabled #SECURITY
