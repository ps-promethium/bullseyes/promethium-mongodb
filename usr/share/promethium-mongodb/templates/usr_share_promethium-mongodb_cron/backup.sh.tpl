#!/bin/bash

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

#
# Notice :
#
# If you want to restore a backup, use also as this one : 
#   mongorestore --username DBUSER --password "[DBPASS]" --authenticationDatabase [DBNAME] --db promethium --drop [DBBACKUPDIR]/[yyyy-mm-dd]/promethium/
#   e.g :
#   mongorestore --username promethium --password "password" --authenticationDatabase promethium --db promethium --drop /var/backups/mongodb/2021-07-17/promethium/
#


DBNAME=promethium
DBUSER=promethium
DBPASS=%%PROMETHIUMPW%%
DBBACKUPDIR=/var/backups/mongodb/
KEEPBACKUPS=7


checkBackupDir() {
    if [ ! -d "$DBBACKUPDIR" ] ; then
        mkdir -p $DBBACKUPDIR
    fi
}


doBackup() {
    logger -t $0 "Start backuping Mongo DB $DBNAME"
    CMD='mongodump --username '$DBUSER' --password "'$DBPASS'" --authenticationDatabase '$DBNAME' --db '$DBNAME' --out '$DBBACKUPDIR$(date +"%Y-%m-%d")
    eval $CMD
    logger -t $0 "Backup Mongo DB $DBNAME done"
}

doPurge() {
    logger -t $0 "Start deleting old Mongo DB backup(s), over $KEEPBACKUPS days"
    CMD='find '$DBBACKUPDIR' -mtime +'$KEEPBACKUPS' -exec rm -rf {} \;'
    eval $CMD
    logger -t $0 "Old backup(s) Deletion done"
}

checkBackupDir
doBackup
doPurge


exit 0
