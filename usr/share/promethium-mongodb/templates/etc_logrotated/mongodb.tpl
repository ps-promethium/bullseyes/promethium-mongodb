/var/log/mongodb/mongod.log
{
    rotate 4
    daily
    size 100M
    missingok
    create 0600 mongodb mongodb
    delaycompress
    compress
    sharedscripts
    postrotate
        /usr/bin/pkill -SIGUSR1 mongod
    endscript
}
