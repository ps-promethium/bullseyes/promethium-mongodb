#!/bin/bash

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


version=1.0.0

#
# Because we need administrative rights
#

if [ $(whoami) != "root" ] ; then echo "This script must be launched with superuser administrative rights ! Bye" ; exit 1 ;  fi




############################################################################################################################################################################################# 
# Local definition                                                                                                                                                                          #
############################################################################################################################################################################################# 
FQDN=$(hostname -f)
LOCALIP=$(hostname -I |cut -d" " -f1)

PRIMARYNODE=""
ISPRIMARYNODE=false
CONFTYPE=""
NODESLIST=""


#############################################################################################################################################################################################             
# Options list                                                                                                                                                                              #
############################################################################################################################################################################################# 

TEMP=`getopt -o t:n:a:p:hvC -l confType:,nodesList:,adminMongoPwd:,promethiumAcountPwd:,help,version,checkenv -n "$(basename $0)" -- "$@"`
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

eval set -- "$TEMP"

esc=$(printf '\033')

#############################################################################################################################################################################################             
# Local functions                                                                                                                                                                           #
############################################################################################################################################################################################# 

#
# Display "version" 
#
version () {
        echo -e ""
        echo -e "\tAbout \"$(basename $0)\" : "
        echo -e "\t\tVersion : $version"
        echo -e "\t\tLicense : GPL V3"
        echo -e "\t\tAuteur  : Pascal SALAUN <pascal.salaun@mclabosse.fr>"
        echo -e ""
}


#
# Display "usage" 
#
usage () {
        echo -e "\n  USAGE of \"$(basename $0)\"\n"
        echo -e ""
        echo -e "\t-v, --version : Display the version "
        echo -e ""
        echo -e "\t-h, --help || ? : Display this help"
        echo -e ""
        echo -e "\t-C, --checkenv || ? : check this environment"
        echo -e ""
        echo -e "\tOtherwise, you have to use it as :"
        echo -e "\t $0  -t|--confType (Standalone|Distributed) -n|--nodesList "
                
        echo -e "" 
        echo -e "\tWhere"
      
        echo -e "" 
        echo -e "\t -t|--confType :"
        echo -e "\t\t\t is the type of server. Could be Standalone or Distributed"

        echo -e "" 
        echo -e "\t -n|--nodesList :"
        echo -e "\t\t\t is the list of IPs of all cluster members, Master one first, and space separated"
        echo -e "\t\t\t as e.g : 1.2.3.4/24 1.2.3.5/24 1.2.3.6/24"
        
        echo -e "" 
        echo -e "\t -a|--adminMongoPwd :"
        echo -e "\t\t\t is the mongDb admin password"

        echo -e "" 
        echo -e "\t -p|--promethiumAcountPwd :"
        echo -e "\t\t\t is the promethium account password"                
        }

#
# Display few env elements
#
checkEnv () {
    
        firewalldStatus=$(service firewalld status |grep -v grep| grep Active: > /tmp/1 ; cat /tmp/1 | tr -s " " | sed "s/Active: //" ; rm -f /tmp/1)
        selinuxConf=$(grep -e '^SELINUX=' /etc/sysconfig/selinux | cut -d "=" -f2)
        
        
        if [ -z "$selinuxConf" ] ; then 
                selinuxConf='Not set'
        fi
        
        Java=$(java -version 2>&1 >/dev/null | grep 'version' | awk '{print $3}')
        echo -e ""
        echo -e "\t Please, before configuring Filebeat, check if these values are correct "
        echo -e ""
        echo -e "\t\t hostname        : $hostname"
        echo -e "\t\t firewalld       :$firewalldStatus"
        echo -e "\t\t SELINUX         : $selinuxConf"
        echo -e ""

}
        
        
# 
# Strip all spaces (beginning, ending, multiple) 
#
stripSpaces () {
    CMD="echo \"$1\"" 
    CMD=$CMD" | tr -s ' ' "
    CMD=$CMD" | sed -e 's/^ //' -e 's/ $//'"
    eval $CMD
}


#
# Set system configuration (swappiness...)
#
setInfraSpec() {
    echo "Setting systcl params"  
    sed -i '/swappiness/d' /etc/sysctl.conf
    echo "vm.swappiness = 1" >> /etc/sysctl.conf
    sysctl -p
    
    systemctl start disableTHP
    systemctl enable disableTHP
}

## mongoDb post install stuff here
#
# Configure /etc/mongod.conf
#
configureMongoDb () {
    echo -e ""
    echo -e "Applying new configuration file(s)"
    for f in `cd /usr/share/promethium-mongodb/templates/etc ; ls *.tpl` ; do 
        echo -e "" 
        echo -e "File : /usr/share/promethium-mongodb/templates/etc/"$f
        F=${f%.*}
        
        CMD="cp /etc/$F /etc/$F.$(date +%F-%H%M%S)"             
        eval $CMD
        
        CMD="cp /usr/share/promethium-mongodb/templates/etc/$f /etc/$F"             
        eval $CMD
    
        CMD="sed -i "
        CMD=$CMD" -e 's;%%HOSTNAME%%;$FQDN;'" 
        CMD=$CMD" /etc/$F "
        eval $CMD    
    done
    
    if [ "$CONFTYPE" = "Standalone" ] ; then 
        sed -i '/DISTRIBUTED/d' /etc/mongod.conf
    fi
    
    sed -i 's/#DISTRIBUTED//'  /etc/mongod.conf
}


#
# Set a secret key only known by the nodes
#
setMongoKeyfile () {
    if [ ! -d "/etc/mongo" ] ; then 
        mkdir /etc/mongo
    fi
    
    chmod 400 /etc/mongo/mongo-keyfile
    chown mongodb:mongodb /etc/mongo -R

    if $ISPRIMARYNODE ; then
        echo -e ""
        echo -e ""
        echo -e "A generic \"/etc/mongo/mongo-keyfile\" is provided by this package"
        echo -e "Please, generate a new one and copy/paste it on each node" 
        echo -e "The command is : "
        echo -e ""
        echo -e "\t openssl rand -base64 756 > mongo-keyfile"
        echo -e ""
        echo -e "Pay attention to owner and rights when copy"
    fi
}


#
# Check if this host is primary MongoDb node.
# In this case, will do some stuff (creating indexex...)
#
controlIfPrimaryNode () {
    PRIMARYNODE=$(echo "$NODESLIST"|cut -d " " -f1)
    if [[ "$LOCALIP" == *"$PRIMARYNODE"* ]] || [[ "$FQDN" == *"$PRIMARYNODE"* ]] ; then
        ISPRIMARYNODE=true
    fi
}



#
# Create a file "/tmp/mongodb_nodes"containing nodes info
#
initClusterCommandFile () {
    nodeMembers=$(stripSpaces "$NODESLIST")

    touch /tmp/promethium_mongo_set_cluster
    > /tmp/promethium_mongo_set_cluster

    n=0
    echo 'config = { _id : "promethiumMongo0",  members : [' >>  /tmp/promethium_mongo_set_cluster
    for HST in $nodeMembers ; do
        echo "{_id : "$n", host : \""$HST":27017\"}," >>  /tmp/promethium_mongo_set_cluster
        n=$((n+1))
    done 
    echo ']}' >>  /tmp/promethium_mongo_set_cluster
    echo 'rs.initiate(config)'  >> /tmp/promethium_mongo_set_cluster

}

#
# Apply /tmp/mongodb_nodes content and reset it
#
applyClusterCommandFile() {
    echo -e ""
    echo -e ""
    echo -e "Setting the cluster"
    CMD='mongosh --username admin --password "'$MONGOADMINPW'" --authenticationDatabase admin --host "'$LOCALIP'"< /tmp/promethium_mongo_set_cluster'
    eval $CMD

    echo -e ""
    echo -e "First : "  
    echo -e "\t On each secondary node, apply command :"
    echo -e "\t\t  service mongod stop && rm -fr /var/lib/mongodb/* && service mongod start"
    echo -e ""
    echo -e "Second : "
    echo -e "\t On the Primary node replay the cluster node subscription : "
    echo -e "\t\t "$CMD
    echo -e ""
    echo -e ""
}

displayTestCommand() {
    echo -e "Install is now done."
    echo -e "You can test you connection to mongodb with these commands :"
    echo -e ""
    echo -e "\t mongosh --username admin --password "\"$MONGOADMINPW\"" --authenticationDatabase admin --host $LOCALIP"
    echo -e "\t mongosh --username promethium --password "\"$PROMETHIUMPW\"" --authenticationDatabase promethium --host $LOCALIP"
}


## Promethium DB stuff here
#
# Create a file "/tmp/promethium_mongo.init" containing account info
#s
initDbConfigurationFile () {
    touch /tmp/promethium_mongo.init
    > /tmp/promethium_mongo.init    
    
    echo 'use admin' >> /tmp/promethium_mongo.init
    echo 'db.createUser({ user: "admin", pwd: "'$MONGOADMINPW'", roles: [{ role: "root", db: "admin" }] })' >> /tmp/promethium_mongo.init
    echo 'use promethium' >> /tmp/promethium_mongo.init
    echo 'db.createUser({ user: "promethium", pwd: "'$PROMETHIUMPW'", roles: [{ role: "dbOwner", db: "promethium" }] })' >> /tmp/promethium_mongo.init
}

#
# Apply /tmp/promethium_mongo.init content and reset it
#
applyDbConfiguration() {
    echo -e ""
    echo -e ""
    echo -e "Create accounts and promethium Db from /tmp/promethium_mongo.init"
    mongosh  < /tmp/promethium_mongo.init
    >  /tmp/promethium_mongo.init
}

#
# Uncomment security bloc
# It supposes you've published mongor secret key file on each node
#
setSecurityBloc() {
    sed -i -e '/#SECURITY/s/#//' -e 's/#SECURITY//'  /etc/mongod.conf
}


#
# Configure /usr/share/promethium-mongodb/cron/backup.sh
#
setMongodbBackupScript() {
    echo -e ""
    echo -e "Setting Backup script"
    for f in `cd /usr/share/promethium-mongodb/templates/usr_share_promethium-mongodb_cron ; ls *.tpl` ; do 
        echo -e "" 
        echo -e "File : /usr/share/promethium-mongodb/templates/usr_share_promethium-mongodb_cron/"$f
        F=${f%.*}
        
        if [ -f "/usr/share/promethium-mongodb/cron/$F" ] ; then
            CMD="cp /usr/share/promethium-mongodb/cron/$F /usr/share/promethium-mongodb/cron/$F.$(date +%F-%H%M%S)"             
            eval $CMD
        fi 
        
        CMD="cp /usr/share/promethium-mongodb/templates/usr_share_promethium-mongodb_cron/$f /usr/share/promethium-mongodb/cron/$F"             
        eval $CMD
    
        CMD="sed -i "
        CMD=$CMD" -e 's;%%PROMETHIUMPW%%;$PROMETHIUMPW;'" 
        CMD=$CMD" /usr/share/promethium-mongodb/cron/$F "
        eval $CMD    
        
        chmod -x /usr/share/promethium-mongodb/cron/$F
    done
}

#
# Configure /etc/cron.d/mongoBackup
#
setCronMongodbBackupScript() {
    echo -e ""
    echo -e "Setting CRON Backup script"
    for f in `cd /usr/share/promethium-mongodb/templates/etc_crond ; ls *.tpl` ; do 
        echo -e "" 
        echo -e "File : /usr/share/promethium-mongodb/templates/etc_crond/"$f
        F=${f%.*}
        
        if [ ! -f /etc/cron.d/mongoBackup ] ; then       
            CMD="cp /usr/share/promethium-mongodb/templates/etc_crond/$f /etc/cron.d/$F"             
            eval $CMD
        fi
        chmod -x /etc/cron.d/$F
    done

}



#
# Set logrotation for mongodb
#

setMongodbLogRotation() {
    echo ""
    echo "Define mongodb log rotation " 
    if [ ! -f /etc/logrotate.d/mongodb ] ; then
        cp /usr/share/promethium-mongodb/templates/etc_logrotated/mongodb.tpl /etc/logrotate.d/mongodb
        chmod 644 /etc/logrotate.d/mongodb
        echo "You can configure /etc/logrotate.d/mongodb with your own values"
    fi
}

# 
# MAIN 
#
while true ; do
    case "$1" in
        -v | --version )
            version
            exit ;;
            
        ? | -h | --help )
            usage
            exit ;;
            
        -C | --checkenv )
            checkEnv
            exit ;;
            
        -t|--confType )
            CONFTYPE=$(echo $2) ; shift 2 ;;
            
        -n|--nodesList )
            NODESLIST=$(echo $2) ; shift 2 ;;
            
        -a|--adminMongoPwd )
            MONGOADMINPW=$(echo $2) ; shift 2 ;;
            
        -p|--promethiumAcountPwd )
            PROMETHIUMPW=$(echo $2) ; shift 2 ;;
            
        --) shift; break ;;
        *) break ;;
    esac
done


if [ "$CONFTYPE" = "" ] ; then 
    usage
    exit 1
fi

if [[ "$CONFTYPE" == "Distributed" &&  "$NODESLIST" == "" ]] ; then 
    usage
    exit 1
fi


if [ "$CONFTYPE" = "Standalone" ] ; then 
    NODESLIST=$FQDN
fi

systemctl start mongod
echo "Waiting 2 sec. while mongod starting"
sleep 2

controlIfPrimaryNode

# On all node(s)
setInfraSpec
configureMongoDb
setSecurityBloc
setMongoKeyfile



# On Primary only
if $ISPRIMARYNODE ; then
    initDbConfigurationFile
    initClusterCommandFile
    applyDbConfiguration
    
    echo "Restart to apply the new configuration"
    systemctl stop mongod
    echo "Waiting 2 sec. while mongod stopping"   
    sleep 2
    systemctl start mongod
    echo "Waiting 2 sec. while mongod starting"
    sleep 2
    if [ "$CONFTYPE" = "Distributed" ] ; then
        applyClusterCommandFile
    fi
else
    systemctl stop mongod
    echo "Waiting 2 sec. while mongod stopping"   
    sleep 2
    systemctl start mongod
fi 

setMongodbLogRotation
setMongodbBackupScript
setCronMongodbBackupScript

systemctl enable mongod

displayTestCommand

exit 0
