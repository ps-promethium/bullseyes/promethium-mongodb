#!/bin/bash

#  +-- promethium ----------------------------------------------------------------+
#  |                                                                              |
#  |                                     _    _                                   |
#  |  ____   _ _   ___   _ __ ___   ___ | |_ | |__  _  _   _  _ __ ___            |
#  | |  _  \| `_| / _ \ | '_ ` _ \ / _ \|  _||  _ \| || | | || '_ `   \           |
#  | | |_| || |  | |_| || | | | | |  __/| |  | | | | || |_| || | | | | |          |
#  | | ____/|_|   \___/ |_| |_| |_|\___||_|  |_| |_|_| \___/ |_| |_| |_|          |
#  | | |                                                                          |
#  | |_|                                                                          |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * promethium is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  promethium is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*

updateMongodbLogRotation() {
    if [ -f /etc/logrotate.d/mongodb ] ; then
        if [ $(grep -c "cat" /etc/logrotate.d/mongodb) == 1 ] ; then 
            echo ""
            echo "Reset the /etc/logrotate.d/mongodb with maintener's one"
            cat /usr/share/promethium-mongodb/templates/etc_logrotated/mongodb.tpl > /etc/logrotate.d/mongodb
        fi
    fi
}

setMongodbLogRotation() {
    if [ ! -f /etc/logrotate.d/mongodb ] ; then
        echo ""
        echo "Define mongodb log rotation " 
        cp /usr/share/promethium-mongodb/templates/etc_logrotated/mongodb.tpl /etc/logrotate.d/mongodb
        chmod 644 /etc/logrotate.d/mongodb
        echo "You can configure /etc/logrotate.d/mongodb with your own values"
    fi
}



chmod -x /lib/systemd/system/disableTHP.service
chown -R mongodb:mongodb /etc/mongo 
chmod 400 /etc/mongo/mongo-keyfile

updateMongodbLogRotation
setMongodbLogRotation


exit 0
